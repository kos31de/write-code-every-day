# 100円の商品に10％の消費税を加えた販売価格110円かどうか判定せよ。

# 浮動小数点数を比較する
def tax
  puts 100 * 1.1 == 110
end

# 整数インスタンスに変換する
def tax_round
  puts (100 * 1.1).round == 110
end

# 有理数インスタンスを使う
def tax_rational
  puts 100 * 1.1r == 110
end

tax
tax_round
tax_rational
