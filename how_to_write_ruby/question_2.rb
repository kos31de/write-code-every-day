# 数10, 11, 12, ..., 20の順にKernel#putsメソッドにより出力せよ

def test
  i = 10
  while i <= 20
    puts i
    i += 1
  end
end

# succメソッドは、「次の文字列」を返します。nextメソッドはsuccの別名です。
def test_increment
  i = 10
  while i <= 20
    puts i
    i = i.succ
  end
end

# Rubyで整数を明示的にインクメントする必要があることは少ない。イテレータを使おう。
def test_iterator
  10.upto(20).each{ |i| puts i }
end
test_iterator
