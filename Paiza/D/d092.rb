require  'bigdecimal'

data1 = gets.chomp.split.map(&:to_i)
data2 = gets.chomp.split.map(&:to_i)
data1_price = data1[2] / (data1[0] * data1[1]) 
data2_price = data2[2] / (data2[0] * data2[1]) 
BigDecimal(data1_price)
BigDecimal(data2_price)

if data1_price  >= data2_price
    puts data1.join(' ')
elsif data2_price  >= data1_price
    puts data2.join(' ')
else data1_price == data2_price
    puts "DRAW"
end
