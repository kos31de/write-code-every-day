# mapは配列に入れて、その各要素にメソッドを適用する
a, b = gets.split(" ").map(&:to_i)
c, d = gets.split(" ").map(&:to_i)
puts a * d - b * c
