## 注意点
### ファイル名をナンバーではなく名前にする

### スペース区切り１行で入力を受け取る 
```ruby
a, b  = gets.chomp.split(" ").map(&:to_i)
```

### 改行区切りのn行目までの入力をint型で配列に格納する
```ruby
lines = []
  while line = gets do
    lines << line.to_i
  end
```

### 配列の要素の数だけインデックスと共に繰り返し出力
```ruby
animals = ["dog", "cat", "mouse"]
animals.each_index {|idx| print "#{idx}. #{animals[idx]}" }
# => 0. dog1. cat2. mouse
```

### 浮動小数点の扱い
```ruby
require 'bigdecimal'
# 文字列として渡すのがポイント。ceilは切り上げ
puts (BigDecimal("1800") * BigDecimal("1.08")).ceil
```

### 二重配列の扱い
要素の追加はpushか<<
```ruby
elements = [["UCLA","UCB"],["UCSD","UCSF","MIT"]]
elements[0].push("Harvard") # elements[0] << "Harvard" でもok
p elements[0]
=> ["UCLA", "UCB", "Harvard"]
```
要素の削除
```ruby
elements = [["UCLA","UCB"],["UCSD","UCSF","MIT"]]
elements[1].delete_at(1)
p elements
=> [["UCLA", "UCB"], ["UCSD", "MIT"]]
```
