=begin
消費税を計算する問題で、ハマった。浮動小数点問題
irb(main):001:0> 1800 * 1.08
=> 1944.0000000000002
=end
require 'bigdecimal'
# 文字列として渡すのがポイント。ceilは切り上げ
puts (BigDecimal("1800") * BigDecimal("1.08")).ceil
# => 1944
