# 合計の球数を入力する
a = gets.to_i
# strikeとballの変数を用意して、初期値を0にセット
 strike = 0
 ball = 0
# strikeとballの数はa-1回繰り返す
for i in 1..a-1 do
# for文の中で変数受け取れる 
    b =	 gets.chomp
# strikeならstrike!とcallしてstrike変数に1を足す    
    if b == "strike" 
       puts "strike!"
       strike = strike + 1
# ballの場合もstrikeと同様 
    elsif b == "ball" 
       puts "ball!"
       ball = ball + 1
     end
end

c = gets.chomp
# strikeかballの文字列の入力を受け取る
if c == "strike" 
   strike = strike + 1
elsif c == "ball" 
   ball = ball + 1
end
# strikeが3になったらout!を出力
if strike == 3
   puts "out!"
end
# ballが4になったらfourball!を出力
if ball == 4 
   puts "fourball!"
end
