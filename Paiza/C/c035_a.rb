# 人数を取得
num = gets.chomp.to_i
# 点数は配列に入れる
ten = []
# 合格者の数を0
ok = 0
# 不合格者の数を0でセット
ok2 = 0

# 人数の回数だけ点数を取得
 num.times do |i|
  # aに入力値を入れる
	a = gets.chomp
	a = a.split(" ")
  # 文系の場合、(aの配列の0番目がlならば)
   if a[0] == "l"
    # 文系の判定に使ったら配列から削除
    a.delete_at(0)
    # 残りの点数はint型に変換
    ten = a.map(&:to_i)
    # tenを全て足して点数の合計sumを算出
    sum = ten.inject(:+)
    # 文系で合計が350以上の場合で
		if sum >= 350
			l = ten[3] + ten[4]
    # その中でも国語と地理歴史が160点以上ならば、合格者の数を1足して上書き
 			if l >= 160
				ok = ok + 1
			end
		end
  # 理系の場合
 	elsif a[0] == "s"
		a.delete_at(0)
		ten = a.map(&:to_i)
		sum = ten.inject(:+)

 		if sum >= 350
			s = ten[1] + ten[2]

 			if s >= 160
				ok2 = ok2 + 1
			end
		end
	end
end


 puts ok + ok2 
