=begin
入力される値
2
2,5
3,4

期待する出力
hello = 2 , world = 5 
hello = 3 , world = 4
=end

# paizaに書いてある標準入出力
input_lines = gets.to_i
input_lines.times {
  s = gets.chomp.split(",")
  print "hello = ",s[0]," , world = ",s[1],"\n"
}

# 1行目で空白で区切られた2つの数字を受け取り、int型で配列に格納する
a, b = gets.chomp.split(" ").map(&:to_i)
puts a * b

# 改行区切りのn行目までの入力をint型で配列に格納する
lines = []
  while line = gets do
    lines << line.to_i
  end

# 同上
while gets
  record = Hash[$_.split("\t").map{|f| f.split(":", 2)}]
  p record
end
