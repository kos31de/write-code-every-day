# なぜZeroDivisionErrorになるのか
backup_require, hdd_volume,  hdd_price = gets.chomp.split("").map(&:to_i)
per = backup_require / hdd_volume
if per == 1
    puts hdd_price
else
    puts per
end
