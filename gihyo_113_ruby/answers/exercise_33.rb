module Answer1
  def code
    inch = 100_000
    feet, inch = inch.divmod(12)
    yard, feet = feet.divmod(3)
    mile, yard = yard.divmod(1760)
    [mile, yard, feet, inch]
  end
end

module Answer2
  def code
    [12, 3, 1760]
    .inject([100_000]){|(r, *a), d| r.divmod(d) + a}
  end
end
