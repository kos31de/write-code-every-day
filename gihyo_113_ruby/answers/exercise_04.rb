module Answer1
  def code
    loop{break if gets == "exit"}
  end
end

module Answer2
  def code
    loop{break if gets(chomp: true) == "exit"}
  end
end
