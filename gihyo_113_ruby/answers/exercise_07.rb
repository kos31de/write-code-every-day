module Answer1
  def code
    "MINASWAN".split("")
  end
end

module Answer2
  def code
    "a programmer's best friend".split(/\s+/)
  end
end

module Answer3
  def code
    "foo\nbar\nbaz\n".split("\n")
  end
end

module Answer4
  def code
    "a programmer's best friend".split
  end
end

module Answer5
  def code
    "MINASWAN".chars
  end
end

module Answer6
  def code
    "foo\nbar\nbaz\n".lines(chomp: true)
  end
end
