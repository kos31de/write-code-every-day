module Setting1
  def array
    @array ||= [["×", "×", "○"], ["○", "○", "×"], ["×", "○","×"]]
  end
end

module Setting2
  def hash
    @hash ||=
    {name: ["田中", nil, "太郎"], address: ["東京都", ["港区", ["芝浦"]]], age: [20], membership: [false]}
  end
end

module Answer1
  def code
    hash.transform_values(&:flatten)
  end
end

module Answer2
  def code
    array.flatten
  end
end

module Answer3
  def code
    array.flat_map(&:itself)
  end
end
