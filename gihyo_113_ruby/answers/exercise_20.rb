module Setting1
  def hash1
    @hash1 ||=
    {"Perl" => "Warhol",
    "Python" => "van Rossum",
    "Ruby" => "Mazt"}
  end

  def hash2
    @hash2 ||= {"Perl" => "Wall", "Ruby" => "Matz"}
  end
end

module Answer1
  def code
    hash1 = hash1()
    hash2.each{|k, v| hash1[k] = v}
    hash1
  end
end

module Answer2
  def code
    hash1.merge!(hash2)
  end
end
