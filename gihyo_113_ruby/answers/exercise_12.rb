module Setting1
  def array; @array ||= [1, 1] end
end

module Setting2
  def hash; @hash ||= {1 => 1, 2 => 1} end
end

module Answer1
  def code
    (10 - 2).times.inject(array){|a, _|
      a + [a[-2] + a[-1]]
    }
  end
end

module Answer2
  def code
    (10 - 2).times.inject(hash){|h, i|
      h.merge(i + 3 => h[i + 1] + h[i + 2])
    }
  end
end

module Answer3
  def code
    (10 - 2).times.inject(array.dup){|a, _|
     a.push(a[-2] + a[-1])
    }
  end
end

module Answer4
  def code
    (10 - 2).times.inject(hash.dup){|h, i|
     h.merge!(i + 3 => h[i + 1] + h[i + 2])
    }
  end
end

module Answer5
  def code
    (10 - 2).times.with_object(array.dup){|_, a|
     a.push(a[-2] + a[-1])
    }
  end
end

module Answer6
  def code
    (10 - 2).times.with_object(hash.dup){|i, h|
     h.merge!(i + 3 => h[i + 1] + h[i + 2])
    }
  end
end
