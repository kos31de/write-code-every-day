module Setting1
  def array; @array ||= %w[foo bar baz] end
end

module Answer1
  def code
    array.find{|x| x.start_with?("b")}.capitalize
  end
end

module Answer2
  def code
    array.find{|x| x.start_with?("c")}.capitalize
  end
end

module Answer3
  def code
    array.find{|x| x.start_with?("b")}&.capitalize
  end
end

module Answer4
  def code
    array.find{|x| x.start_with?("c")}&.capitalize
  end
end
