module Setting1
  def keys
    @kays ||=
    %w[garnet amethyst aquamarine diamond
    emerald pearl ruby periodot sapphire opal
    topaz turquoise]
  end
end

module Setting2
  def array
    @array ||=
    %w[garnet amethyst bloodstone sapphire
    agate emerald onyx carnelian chrysolite beryl
    topaz ruby]
  end
end

module Setting3
  def hash
    @hash ||=
    {"garnet" => "Aquarius",
    "amethyst" => "Pisces", "bloodstone" => "Aries",
    "sapphire" => "Taurus", "agate" => "Gemini",
    "emerald" => "Cancer", "onyx" => "Leo",
    "carnelian" => "Virgo", "chrysolite" => "Libra",
    "beryl" => "Scorpio", "topaz" => "Sagittarius",
    "ruby" => "Capricorn"}
  end
end

module Answer1
  def code
    keys & array
  end
end

module Answer2
  def code
    hash.slice(*keys)
  end
end
