module Answer1
  def code
    s = "A"
    (2019 - 1).times{s.succ!}
    s
  end
end

module Answer2
  ALPHABET = ("A".."Z").to_a

  def code
    s, q = "", 2019
    loop do
      q, r = (q - 1).divmod(26)
      s.prepend(ALPHABET[r])
      break s if q.zero?
    end
  end
end
