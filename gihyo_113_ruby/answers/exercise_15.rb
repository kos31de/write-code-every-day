module Setting1
  def teeth
    @teeth ||=
    {"切歯" => {"中_" => 1, "側_" => 2},
      "犬歯" => 3,
      "臼歯" => {"小_" => [4, 5],
      "大_" => [6, 7, 8]}}
  end
end

module Setting2
  def keys_1; @keys_1 ||= ["臼歯", "大_", 2] end
end

module Setting3
  def keys_2; @keys_2 ||= ["切歯", "小_", "側_"] end
end

module Answer1
  def code
    teeth["臼歯"]["大_"][2]
  end
end

module Answer2
  def code
    teeth["切歯"]["小_"]["側_"]
  end
end

module Answer3
  def code
    teeth
    .fetch("切歯", {}).fetch("小_", {}).fetch("側_", nil)
  end
end

module Answer4
  def code
    teeth
    .fetch("臼歯", {}).fetch("大_", {}).fetch(2, nil)
  end
end

module Answer5
  def code
    teeth.dig(*keys_1)
  end
end

module Answer6
  def code
    teeth.dig(*keys_2)
  end
end
