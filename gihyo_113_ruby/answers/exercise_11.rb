module Answer1
  def code
    (1..5).map{|i| i**2}
  end
end

module Answer2
  def code
    (1..5).to_h{|i| [i, i**2]}
  end
end

module Answer3
  def code
    Array.new(5){|i| (i + 1)**2}
  end
end
