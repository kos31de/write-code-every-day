module Setting1
  def string; @string ||= "|l|l|llll||" end
end

module Setting2
  def a; @a ||= "|l|l|lll||" end
end

module Setting3
  def b; @b ||= "||l" end
end

module Setting4
  def c; @c ||= "l||" end
end

module Answer1
  def code
    string.match?(/\A#{Regexp.escape(a)}\z/)
  end
end

module Answer2
  def code
    string.match?(/\A#{Regexp.escape(b)}/)
  end
end

module Answer3
  def code
    string.match?(/#{Regexp.escape(c)}\z/)
  end
end

module Answer4
  def code
    string == a
  end
end

module Answer5
  def code
    string.start_with?(b)
  end
end

module Answer6
  def code
    string.end_with?(c)
  end
end
