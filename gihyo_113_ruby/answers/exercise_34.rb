module Answer1
  def code
    [10000, 5000, 2000, 1000, 500, 100, 50, 10, 5]
    .inject([37482]){|(*a, r), d| a + r.divmod(d)}
  end
end
