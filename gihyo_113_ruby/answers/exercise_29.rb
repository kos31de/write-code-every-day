module Answer1
  def code
    array = Array.new(3, "foo")
    array[0] << "bar"
    array
  end
end

module Answer2
  def code
    array = Array.new(3){"foo"}
    array[0] << "bar"
    array
  end
end
