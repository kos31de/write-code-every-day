module Answer1
  def code
    i = 10
    while i <= 20
      puts i
      i += 1
    end
  end
end

module Answer2
  def code
    i = 10
    while i <= 20
      puts i
      i = i.succ
    end
  end
end

module Answer3
  def code
    10.upto(20).each{|i| puts i}
  end
end
