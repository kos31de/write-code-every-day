module Answer1
  def code
    "03-1234-5678".scan(/\d+/).first
  end
end

module Answer2
  def code
    "03-1234-5678".split("-", 2).first
  end
end

module Answer3
  def code
    "03-1234-5678".partition("-").first
  end
end
