module Setting1
  def array
    @array ||= [1, 17, 14, 9, 3, 16, 15, 3, 18, 13]
  end
end

module Answer1
  def code
    array.sort
  end
end

module Answer2
  def code
    array.sort{|x, y| y <=> x}
  end
end

module Answer3
  def code
    array.sort{|x, y| Math.cos(x) <=> Math.cos(y)}
  end
end

module Answer4
  def code
    array.sort.reverse
  end
end

module Answer5
  def code
    array.sort_by{|x| Math.cos(x)}
  end
end
