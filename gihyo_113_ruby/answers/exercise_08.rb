module Setting1
  def string
    @string ||=
    %{"Come, Watson, come!" he cried. "The game is afoot. Not a word! Into your clothes and come!}
  end
end

module Answer1
  def code
    string.split(/[,. !"]+/).reject(&:empty?)
  end
end

module Answer2
  def code
    string.scan(/[\w']+/)
  end
end
