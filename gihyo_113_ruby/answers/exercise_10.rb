module Setting1
  def strings; @strings ||= ["foo", "Bar", "BAZ"] end
end

module Answer1
  def code
    strings.map!{|s| s.upcase.reverse}
  end
end

module Answer2
  def code
    strings.each{|s| s.upcase!.reverse!}
  end
end

module Answer3
  def code
    strings.each{|s| s.upcase!; s.reverse!}
  end
end
