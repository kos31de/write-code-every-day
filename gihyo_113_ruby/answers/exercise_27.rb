module Setting1
  def n; @n ||= 10 end
end

module Setting2
  def n; @n ||= 100 end
end

module Answer1
  def fibonacci(n)
    return 1 if n == 1 or n == 2
    fibonacci(n - 1) + fibonacci(n - 2)
  end

  def code
    fibonacci(n)
  end
end

module Answer2
  def jacobsthal(n)
    return 1 if n == 1 or n == 2
    jacobsthal(n - 1) + 2 * jacobsthal(n - 2)
  end

  def code
    jacobsthal(n)
  end
end

module Answer3
  def reset; @fibonacci = nil end

  def fibonacci(n)
    @fibonacci ||= {}
    return @fibonacci[n] ||= 1 if n == 1 or n == 2
    @fibonacci[n] ||= fibonacci(n - 1) + fibonacci(n - 2)
  end

  def code
    reset
    fibonacci(n)
  end
end

module Answer4
  def reset; @jabocsthal = nil end

  def jacobsthal(n)
    @jacobsthal ||= {}
    return @jacobsthal[n] ||= 1 if n == 1 or n == 2
    @jacobsthal[n] ||= jacobsthal(n - 1) + 2 * jacobsthal(n - 2)
  end

  def code
    reset
    jacobsthal(n)
  end
end

module Answer5
  def jacobsthal(n)
    (2**n - (-1)**n) / 3
  end

  def code
    jacobsthal(n)
  end
end

module Answer6
  def fibonacci(n)
    alpha = (1 + Math.sqrt(5)) / 2
    beta = (1 - Math.sqrt(5)) / 2
    ((alpha**n - beta**n) / (alpha - beta)).round
  end

  def code
    fibonacci(n)
  end
end
