module Setting1
  def x; @x ||= "foo" end
  def y; @y ||= "bar" end
end

module Answer1
  def code
    x = x()
    y = y()
    x = y
    y = x
    [x, y]
  end
end

module Answer2
  def code
    x = x()
    y = y()
    tmp = x
    x = y
    y = tmp
    [x, y]
  end
end

module Answer3
  def code
    x = x()
    y = y()
    x, y = y, x
    [x, y]
  end
end
