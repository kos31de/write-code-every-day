module Setting1
  def primes
    @primes ||=
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
    47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
  end
end

module Answer1
  def code
    (1..10).to_a.repeated_combination(3)
    .select{|x, y, z| x + y == z}.length
  end
end

module Answer2
  def code
    (1..10).to_a.repeated_combination(3)
    .select{|x, y, z| x**2 + y**2 == z**2}.length == 1
  end
end

module Answer3
  def code
    (1..10).to_a.repeated_combination(3)
    .select{|x, y, z| x**3 + y**3 == z**3}.length == 0
  end
end

module Answer4
  def code
    4.step(100, 2)
    .reject{|x|
      primes.select{|y| primes.include?(x - y)}.length > 0
    }
    .length == 0
  end
end

module Answer5
  def code
    (1..10).to_a.repeated_combination(3)
    .count{|x, y, z| x + y == z}
  end
end

module Answer6
  def code
    (1..10).to_a.repeated_combination(3)
    .one?{|x, y, z| x**2 + y**2 == z**2}
  end
end

module Answer7
  def code
    (1..10).to_a.repeated_combination(3)
    .none?{|x, y, z| x**3 + y**3 == z**3}
  end
end

module Answer8
  def code
    4.step(100, 2)
    .all?{|x| primes.any?{|y| primes.include?(x - y)}}
  end
end
