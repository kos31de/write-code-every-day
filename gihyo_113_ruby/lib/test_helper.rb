require "stringio"
require "timeout"

begin
  require "method_source"
rescue LoadError
  puts "メソッド定義を表示するには、`gem install method_source`を実行して下さい。"
end

begin
  require "benchmark/ips"
  Benchmark::IPS::Job.prepend(Module.new do
    def run
      puts
      print "\033[5mBenchmarking...\033[0m"
      silence{super}
      print "\033[2K\033[1A"
    end
  end)
rescue LoadError
  puts "ベンチマークを行うためには、`gem install benchmark-ips`を実行して下さい。"
end

module Kernel
  alias original_gets gets
  alias original_puts puts
  alias original_p p

  def redirect_gets(*objs)
    Test.input.send(:gets, *objs)
  end

  def redirect_puts(*objs)
    objs.each{|obj| Test.output.concat(obj.to_s.chomp, "\n")}
  end

  def redirect_p(*objs)
    objs.each{|obj| Test.output.concat(obj.inspect.chomp, "\n")}
  end

  def gobble(*) end

  def redirect
    alias gets redirect_gets
    alias puts redirect_puts
    alias p redirect_p
    yield
  ensure
    alias gets original_gets
    alias puts original_puts
    alias p original_p
  end

  def silence(input: nil)
    alias gets redirect_gets
    alias puts gobble
    alias p gobble
    yield
  ensure
    alias gets original_gets
    alias puts original_puts
    alias p original_p
  end
end

module Test  
  def self.input; @input end

  def self.output; @output end

  def self.suite(
    settings: [],
    contexts: [],
    answers: [],
    input: nil,
    timeout: nil,
    expected_value: (no_expected_value = true),
    expected_truth_value: nil,
    expected_output: nil,
    expected_message: nil
  )
    display_title
    display_ruby_version
    if Gem.loaded_specs.key?("method_source")
      display_source(settings, section: "Setting")
      display_source(contexts, section: "Context")
      display_source(answers)
    end
    puts nil, "Result:", nil
    benchmarkables = answers.select do |answer|
      print answer.name
      @input = input && StringIO.new(input)
      @output = ""
      sandbox = prepare_sandbox(settings, contexts, answer)
      result = Timeout.timeout(timeout){redirect{sandbox.code}}
      if !no_expected_value
        verify("# =>", result, expected_value)
      elsif !expected_truth_value.nil?
        verify("# =>", result, !result ^ !expected_truth_value ? !result : result)
      elsif expected_output
        verify("# >>", output, expected_output)
      elsif expected_message
        verify("#", "no exception raised", expected_message)
      else
        verify("#")
      end
    rescue => e
      verify("# !>", e.message, expected_message, !expected_message)
      false
    end
    run_benchmark(settings, contexts, benchmarkables)
  end

  def self.display_title
    @source_file ||= File.basename(caller_locations[1].absolute_path)
    @test_no ||= 0
    @test_no += 1
    puts nil, " #@source_file, Suite #@test_no ".center(75, "=")
  end

  def self.display_ruby_version
    puts nil, "Ruby version:", RUBY_DESCRIPTION
  end

  def self.display_source(modules, section: nil)
    puts nil, "#{section}:" if !modules.empty? && section
    modules.each do |modul|
      puts nil, "#{modul.name}:" unless section
      modul.constants.each do |constant|
        puts nil, "#{constant} = #{modul.const_get(constant)}"
      end
      modul.instance_methods(false).each do |method|
        puts nil, modul.instance_method(method).source.gsub(/^  /, "")
      end
    end
  end

  def self.prepare_sandbox(settings, contexts, answer)
    sandbox =
      Class.new
      .include(*settings, *contexts, answer)
      .extend(*settings, *contexts, answer)
      .new
    settings.each do |setting|
      setting.instance_methods(false).each{|m| sandbox.send(m)}
    end
    sandbox
  end

  def self.verify(
    prompt,
    actual = true,
    expected = (no_expectation = true),
    unexpected_exception = false
  )
    success = actual == expected
    feedback =
      if no_expectation
        "\033[33mno expectation\033[0m"
      elsif success
        "\033[32m#{actual.inspect}\033[0m"
      elsif unexpected_exception
        "\033[31m#{actual.inspect}\033[0m"
      else
        "\033[31m#{actual.inspect}\033[0m (expected #{expected.inspect})"
      end
    puts " #{prompt} #{feedback}"
    success
  end

  def self.run_benchmark(settings, contexts, answers)
    return unless answers.length >= 2
    return unless Gem.loaded_specs.key?("benchmark-ips")
    Benchmark.ips(quiet: true, time: 2.5, warmup: 0.5) do |b|
      answers.each do |answer|
        sandbox = prepare_sandbox(settings, contexts, answer)
        b.report(answer.name){sandbox.code}
      end
      b.compare!
    end
    print "\033[1A"
  end
end
