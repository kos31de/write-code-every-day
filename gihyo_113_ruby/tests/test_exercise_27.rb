require_relative "../lib/test_helper"
require_relative "../answers/exercise_27"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer3, Answer6],
  expected_value: 55,
  timeout: 5
)

Test.suite(
  settings: [Setting1],
  answers: [Answer2, Answer4, Answer5],
  expected_value: 341,
  timeout: 5
)

Test.suite(
  settings: [Setting2],
  answers: [Answer2, Answer4, Answer5],
  expected_value: 422550200076076467165567735125,
  timeout: 5
)

Test.suite(
  settings: [Setting2],
  answers: [Answer1, Answer3, Answer6],
  expected_value: 354224848179261915075,
  timeout: 5
)
