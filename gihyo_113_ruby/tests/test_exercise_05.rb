require_relative "../lib/test_helper"
require_relative "../answers/exercise_05"

Test.suite(
  settings: [Setting1, Setting2],
  answers: [Answer1, Answer4],
  expected_value: false
)

Test.suite(
  settings: [Setting1, Setting3],
  answers: [Answer2, Answer5],
  expected_value: false
)

Test.suite(
  settings: [Setting1, Setting4],
  answers: [Answer3, Answer6],
  expected_value: true
)
