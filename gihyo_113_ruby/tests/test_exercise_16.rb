require_relative "../lib/test_helper"
require_relative "../answers/exercise_16"

Test.suite(
  settings: [Setting1],
  answers: [Answer1],
  expected_value: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
)
