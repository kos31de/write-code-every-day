require_relative "../lib/test_helper"
require_relative "../answers/exercise_06"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2],
  expected_message: "No numbers please"
)
