require_relative "../lib/test_helper"
require_relative "../answers/exercise_11"

Test.suite(
  answers: [Answer1, Answer3],
  expected_value: [1, 4, 9, 16, 25]
)

Test.suite(
  answers: [Answer2],
  expected_value: {1=>1, 2=>4, 3=>9, 4=>16, 5=>25}
)
