require_relative "../lib/test_helper"
require_relative "../answers/exercise_22"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2],
  expected_value: {"a" => 3, "b" => 2, "c" => 4}
)
