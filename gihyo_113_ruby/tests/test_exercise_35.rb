require_relative "../lib/test_helper"
require_relative "../answers/exercise_35"

Test.suite(
  settings: [Setting1],
  answers: [Answer1],
  expected_value: ["b", "c", "a"]
)
