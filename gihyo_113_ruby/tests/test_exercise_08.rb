require_relative "../lib/test_helper"
require_relative "../answers/exercise_08"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2],
  expected_value: ["Come", "Watson", "come", "he", "cried", "The", 
    "game", "is", "afoot", "Not","a", "word", 
    "Into", "your", "clothes", "and", "come"]
)
