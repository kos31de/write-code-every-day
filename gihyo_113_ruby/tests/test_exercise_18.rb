require_relative "../lib/test_helper"
require_relative "../answers/exercise_18"

Test.suite(
  settings: [Setting1, Setting2],
  answers: [Answer1],
  expected_value: ["garnet", "amethyst", "emerald", "ruby", "sapphire", "topaz"]
)

Test.suite(
  settings: [Setting1, Setting3],
  answers: [Answer2],
  expected_value: {"garnet"=>"Aquarius", "amethyst"=>"Pisces", "emerald"=>"Cancer", "ruby"=>"Capricorn", "sapphire"=>"Taurus", "topaz"=>"Sagittarius"}
)
