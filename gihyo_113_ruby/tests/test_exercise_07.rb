require_relative "../lib/test_helper"
require_relative "../answers/exercise_07"

Test.suite(
  answers: [Answer1, Answer5],
  expected_value: ["M", "I", "N", "A", "S", "W", "A", "N"]
)

Test.suite(
  answers: [Answer2, Answer4],
  expected_value: ["a", "programmer's", "best", "friend"]
)

Test.suite(
  answers: [Answer3, Answer6],
  expected_value: ["foo", "bar", "baz"]
)
