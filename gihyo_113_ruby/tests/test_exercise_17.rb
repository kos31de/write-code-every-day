require_relative "../lib/test_helper"
require_relative "../answers/exercise_17"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer3],
  expected_value: 13
)

Test.suite(
  settings: [Setting2],
  answers: [Answer2, Answer4],
  expected_value: [10, 55]
)
