require_relative "../lib/test_helper"
require_relative "../answers/exercise_20"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2],
  expected_value: {"Perl"=>"Wall", "Python"=>"van Rossum", "Ruby"=>"Matz"}
)
