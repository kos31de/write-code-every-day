require_relative "../lib/test_helper"
require_relative "../answers/exercise_10"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2, Answer3],
  expected_value: ["OOF", "RAB", "ZAB"]
)
