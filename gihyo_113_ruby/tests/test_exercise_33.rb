require_relative "../lib/test_helper"
require_relative "../answers/exercise_33"

Test.suite(
  answers: [Answer1, Answer2],
  expected_value: [1, 1017, 2, 4]
)
