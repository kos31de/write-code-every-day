require_relative "../lib/test_helper"
require_relative "../answers/exercise_15"

Test.suite(
  settings: [Setting1, Setting2],
  answers: [Answer1, Answer4, Answer5],
  expected_value: 8
)

Test.suite(
  settings: [Setting1, Setting3],
  answers: [Answer2, Answer3, Answer6],
  expected_value: nil
)
