require_relative "../lib/test_helper"
require_relative "../answers/exercise_21"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer3],
  expected_value: "Bar"
)

Test.suite(
  settings: [Setting1],
  answers: [Answer2, Answer4],
  expected_value: nil
)
