require 'rails_helper'

RSpec.describe "ProjectsApis", type: :request do
  let!(:user) { create(:user) }
  let!(:project) { create(:project, owner: user) }
  let!(:other_user) { create(:user) }
  let!(:other_project) { create(:project, owner: other_user) }
  let(:user_params) { {user_email: user.email, user_token: user.authentication_token } }
    
  describe "GET /api/projects" do
    it 'return current user project' do
      sign_in user
      get api_projects_path, params: user_params

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)[0]['user_id']).to eq user.id
    end

    it 'does not return other user projects' do
      sign_in user
      get api_projects_path,  params: user_params

      expect(response).to have_http_status 200
      expect(JSON.parse(response.body).size).to eq 1
    end
  end

  describe 'POST /api/projects' do
    it 'create current user project' do
      sign_in user
      params = attributes_for(:project)

      expect{ post api_projects_path, params: { project: params, **user_params } }.to change(user.projects, :count).by(1)
    end

    it "responds unprocessable_entity" do
      sign_in user
      params = attributes_for(:project, :invalid)
      post api_projects_path, params: { project: params, **user_params }

      expect(response).to have_http_status 422
    end
  end
end
