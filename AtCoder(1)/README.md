# Rubyの標準入力
## 
a1
a2
.
.
aN
```ruby
n = gets.chop.to_i
a = []
n.times { a << gets.to_i }
```

## 
X Y Z
```ruby
x, y, z = gets.chop.split.map(&:to_i)
```

## 
a1 a2 .... aN
```ruby
a = gets.chop.split.map(&:to_i)
```

##

N
a1  b1
a2  b2
.   .
.  .
aN bN

```ruby
n = gets.chop.to_i
a = []
b = []
n.times do
  line = gets.chop.split.map(&:to_i)
  a << line.shift
  b << line
end
```
