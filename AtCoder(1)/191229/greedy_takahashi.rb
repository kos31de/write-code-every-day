A, B, K = gets.chomp.split(" ").map(&:to_i)

# Kの回数だけループするのが無駄。Kが1回でそれぞれ1引けば良い。
# K.times do
#   if A >= 1
#     A -= 1
#   else B >= 1
#     B -= 1
#   end
# end

if A >= K
  puts "#{A - K} #{B}"
# Bの枚数は回数からAの枚数を引いたものになる
elsif B >= (K - A)
  puts "0 #{B - (K - A)}"
else
  puts "0 0"
end

# AとBとをそれぞれ配列の要素にして、0と比べて大きい方を出力する
# puts "#{[a-k,0].max} #{[b-[k-a,0].max,0].max}"
