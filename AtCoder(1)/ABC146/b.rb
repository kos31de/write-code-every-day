# tr(pattern, replace) -> String
#pattern 文字列に含まれる文字を検索し、 それを replace 文字列の対応する文字に置き換えます。
N = gets.chop.to_i
S = gets.chop

# trメソッドの引数
a = [*"A".."Z"]
puts S.tr(a.join, a.rotate(N).join)
