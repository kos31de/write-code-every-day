N, K = gets.chop.split.map(&:to_i)
H = gets.split.map(&:to_i)

if N < K
  puts 0
else
  puts H.inject(:+).to_i - H.max(K).inject(:+).to_i
end
