a, b = gets.chomp.split(' ').map(&:to_i)
# 条件演算子(三項演算子とも呼ばれます)は条件式の結果によって異なる値を返す演算子です。条件式 ? 真の時の値 : 偽の時の値
(a * b) % 2 == 0 ? print("Even\n") : print("Odd\n")
