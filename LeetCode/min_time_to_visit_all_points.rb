def min_time_to_visit_all_points(points)
  time = 0
  (1...points.length).each do |i|
    time += [(points[i-1][0]-points[i][0]).abs, (points[i-1][1]-points[i][1]).abs].max
  end
  time
end
