# @param {Integer} n
# @return {String[]}
# wip
def fizz_buzz(n)
  array = *1..n
    
  array.each { |i|
    if i % 15 == 0
      i = "FizzBuzz"
    elsif i % 5 == 0
      i = "Fizz"
    elsif i % 3 == 0
      i = "Buzz"
    else
      i.to_s
    end
    }
    
    puts array
end
