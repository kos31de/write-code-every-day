# WA
def length_of_last_word(s)
    if s == ""
        0
    else
        length ||= s.slice(/\s.*/)&.size
        length.to_i - 1
    end
end

# スペースだけのときは、早期returnすればよい
# split(" ")で配列にできるので、[-1]で最後の要素をとる
def length_of_last_word(s)
  return 0 if s.strip.empty?
  words = s.split(" ")
  last_word = words[-1]
  last_word.length
end
