def to_lower_case(s)
  s.downcase
end

def to_lower_case(str)
  str.split('').each_with_index do |value, index|
    if !!(/[A-Z]/ =~ value)
      str_asc = value.ord
      low_asc = str_asc + 32
      ac_low_asc = low_asc.chr("UTF-8")
      str[index] = ac_low_asc
    end
  end
  return str
end
