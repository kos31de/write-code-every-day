def odd_cells(n, m, indices)
  input = Array.new(n) {Array.new(m, 0)}
  count = 0

  indices.each_with_index do |x, i|
    r = x[0]
    col = x[1]
    input[r].collect!  { |v| v = v+1 }
    input.each  {|a| a[col] = a[col] + 1 }
  end

  input.each_with_index do |arr, i|
    arr.each do |x|
      count+=1 if x.odd?
    end
  end
    puts input
    puts count
end
odd_cells(2, 2, [0, 1])

# Array.new(配列の個数, 配列の中身)
