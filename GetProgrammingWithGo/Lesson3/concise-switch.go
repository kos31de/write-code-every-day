package main

import ("fmt")

func main()  {
	
	fmt.Println("洞窟の入り口だ。東へ進む道もある。")

	var command = "go east"

	switch command {
	case "go east":
		fmt.Println("きみは、さらに山を登る")
	case "enter cave", "go inside":
		fmt.Println("君は薄暗い洞窟の中にいる")
	case "read sign":
		fmt.Println("未成年立ち入り禁止")
	default:
		fmt.Println("なんだか、よくわからない。")
	}
}

