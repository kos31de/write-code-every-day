package main
import ("fmt")
func main() {
	var room = "lake"
	switch {
	case room == "cave":
		fmt.Println("きみは薄暗い洞窟のなか")
	case room == "lake":
		fmt.Println("こおりがはっている")
		fallthrough //lakeのケースではなく、underwaterのケースが出力される
	case room == "underwater":
		fmt.Println("水は凍るくらいに冷たい。")
	}
}
