package main

import "fmt"

func main() {
	// fmt.Print("火星の表面で、私の体重は、")
	// fmt.Print(139 * 0.3783)
	// fmt.Print("ポンド、年齢は")
	// fmt.Print(27 * 365 / 687)
	// fmt.Print("歳になるでしょう。")

	fmt.Printf("火星の表面で、私の体重は、%vポンド、", 139*0.2783)
	fmt.Printf("年齢は、%v歳になるでしょう。 \n", 27*365/687)
	
	//複数のフォーマットを指定
	fmt.Printf("私の体重は、%vの表面では%vポンドです。\n", "Earth", 139.0)

	//Printfはテキストのアライメントもできる
	fmt.Printf("%-15v $%4v\n", "SpaceX", 94)
	fmt.Printf("%-15v $%4v\n", "Virgin Galactic", 100)

}
