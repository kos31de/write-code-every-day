package main

import "fmt"

func main() {
  const lightspeed = 299792
  var distance = 56000000
  fmt.Println(distance/lightspeed, "秒")

  distance = 401000000
  fmt.Println(distance/lightspeed, "秒")

  distance = 96300000
  fmt.Println(distance/lightspeed, "秒")

  var day_hours, hour_minutes = 24, 60n
}
